#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'jlandicheff'

from setuptools import setup, find_packages

setup(
    name='MaryTTS client',
    version='0.1',
    description='A client for the Mary Text-to-speech server',
    author='Jonathan Landicheff',
    author_email='404serendipity@gmail.com',
    url='https://bitbucket.org/jlandicheff/mtts-client',
    packages=find_packages(),
    install_requires=['pygame','requests']
)