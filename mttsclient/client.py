# -*- coding: utf-8 -*-

import requests
import pygame


__author__ = 'jlandicheff'

class Client:

    HOST = "localhost"
    PORT = 59125
    SOUND_STOP_EVENT_TYPE = 42

    def __init__(self, host=HOST, port=PORT):
        """Ctor for MaryTTS Client

        :param host: server host
        :param port:  port of server host
        """
        self.host = host
        self.port = port
        pygame.mixer.pre_init(frequency=48000, size=-16, channels=1, buffer=4096)
        pygame.init()

    def say(self, text):
        """Speak the given text through the MaryTTS server."""
        f = open("/tmp/say.wav", "wb")
        f.write(requests.get(self.generate_url(text)).content)
        f.close()

        pygame.mixer.music.load("/tmp/say.wav")
        pygame.mixer.music.set_endevent(Client.SOUND_STOP_EVENT_TYPE)
        pygame.mixer.music.play()
        while True:
            e = pygame.event.get()
            if len(e) > 0 and e[0].type == Client.SOUND_STOP_EVENT_TYPE:
                exit()

    def server_url(self):
        """Form the url to the MaryTTS server based on the host and port given"""
        return self.host + ":" + str(self.port)

    def generate_url(self, text):
        """Generate the url to generate and retrieve the audio file."""
        url = "http://%s/process?INPUT_TYPE=TEXT" % self.server_url()
        url += "&OUTPUT_TYPE=AUDIO"

        url += "&INPUT_TEXT="
        url += text.replace(" ", "%20")

        url += "&OUTPUT_TEXT="
        url += "&effect_Volume_selected="
        url += "&effect_Volume_parameters=amount%3A2.0%3B"
        url += "&effect_Volume_default=Default"
        url += "&effect_Volume_help=Help"
        url += "&effect_TractScaler_selected="
        url += "&effect_TractScaler_parameters=amount%3A1.5%3B"
        url += "&effect_TractScaler_default=Default"
        url += "&effect_TractScaler_help=Help"
        url += "&effect_F0Scale_selected=on"
        url += "&effect_F0Scale_parameters=f0Scale%3A2.0%3B"
        url += "&effect_F0Scale_default=Default"
        url += "&effect_F0Scale_help=Help"
        url += "&effect_F0Add_selected=on"
        url += "&effect_F0Add_parameters=f0Add%3A50.0%3B"
        url += "&effect_F0Add_default=Default"
        url += "&effect_F0Add_help=Help"
        url += "&effect_Rate_selected=on"
        url += "&effect_Rate_parameters=durScale%3A1.5%3B"
        url += "&effect_Rate_default=Default"
        url += "&effect_Rate_help=Help"
        url += "&effect_Robot_selected="
        url += "&effect_Robot_parameters=amount%3A100.0%3B"
        url += "&effect_Robot_default=Default"
        url += "&effect_Robot_help=Help"
        url += "&effect_Whisper_selected="
        url += "&effect_Whisper_parameters=amount%3A100.0%3B"
        url += "&effect_Whisper_default=Default"
        url += "&effect_Whisper_help=Help"
        url += "&effect_Stadium_selected="
        url += "&effect_Stadium_parameters=amount%3A100.0"
        url += "&effect_Stadium_default=Default"
        url += "&effect_Stadium_help=Help"
        url += "&effect_Chorus_selected="
        url += "&effect_Chorus_parameters=delay1%3A466%3Bamp1%3A0.54%3Bdelay2%3A600%3Bamp2%3A-0.10%3Bdelay3%3A250%3Bamp3%3A0.30"
        url += "&effect_Chorus_default=Default"
        url += "&effect_Chorus_help=Help"
        url += "&effect_FIRFilter_selected="
        url += "&effect_FIRFilter_parameters=type%3A3%3Bfc1%3A500.0%3Bfc2%3A2000.0"
        url += "&effect_FIRFilter_default=Default"
        url += "&effect_FIRFilter_help=Help"
        url += "&effect_JetPilot_selected="
        url += "&effect_JetPilot_parameters="
        url += "&effect_JetPilot_default=Default"
        url += "&effect_JetPilot_help=Help"
        url += "&HELP_TEXT=&exampleTexts="
        url += "&VOICE_SELECTIONS=cmu-slt-hsmm%20en_US%20female%20hmm"
        url += "&AUDIO_OUT=WAVE_FILE"
        url += "&LOCALE=en_US"
        url += "&VOICE=cmu-slt-hsmm"
        url += "&AUDIO=WAVE_FILE"
        return url

    def quit(self):
        exit(0)
        return